﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Security.Cryptography; 
using Entidades.Proyecto;
using LogicaNegocios.Proyecto;

namespace ProyectoVerano
{
    public partial class Form1 : Form
    {
        LoginManejador _lm;

        public Form1()
        {
            InitializeComponent();
            _lm = new LoginManejador();
          
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            if (txtUs.Text == "Usuario" || txtUs.Text == "")
            {
                mensaje("Introduczca un usuario.");
            }
            else if (txtCon.Text == "Contraseña" || txtCon.Text == "")
            {
                mensaje("Introduzca una contraseña.");
            }
            else if (ValidarCantidadCaracteres(txtUs.Text) < 5)
            {
                mensaje("El usuario debe contar con mas de 5 caracteres.");
            }
            else if (ValidarCantidadCaracteres(txtUs.Text) > 10)
            {
                mensaje("El usuario no debe contar con mas de 10 caracteres.");
            }
            else if (ValidarSimbolos(txtUs.Text) == 1)
            {
                mensaje("El usuario no debe contar con caracteres especiales o simbolos.");
            }
            else if (ingreso(txtUs.Text, Encriptar.GetSHA256("" + txtCon.Text + "")) == 0)
            {
                mensaje("El usuario y/o contraseña es incorrecto.");
               /* mensaje(""+ Encriptar.GetSHA256("" + txtCon.Text + "") + "");*/
            }

            else 
            {
                mensaje("Bienvenido, "+txtUs.Text);

                Frm_menu f = new Frm_menu();
                f.ShowDialog();
                this.Close();
            }

        }

        private int ingreso(string usuario, string contraseña)
        {
            var y = _lm.login(usuario, contraseña);
            return int.Parse(y);
        }

        void mensaje(string mensaje)
        {
            DialogResult Resultado = new DialogResult();
            Form message = new frm_mensaje(mensaje);
            Resultado = message.ShowDialog();
        }

        private void txtUs_Enter(object sender, EventArgs e)
        {
            if (txtUs.Text == "Usuario")
            {
                txtUs.Text = "";
            }
        }

        private void txtUs_Leave(object sender, EventArgs e)
        {
            if (txtUs.Text == "")
            {
                txtUs.Text = "Usuario";
            }
        }

        private void txtCon_Enter(object sender, EventArgs e)
        {
            if (txtCon.Text == "Contraseña")
            {
                txtCon.Text = "";
                txtCon.UseSystemPasswordChar = true;
            }
        }

        private void txtCon_Leave(object sender, EventArgs e)
        {
            if (txtCon.Text == "")
            {
                txtCon.Text = "Contraseña";
                txtCon.UseSystemPasswordChar = false;
            }
        }
        
        public int ValidarSimbolos(string dato)
        {
            int v = 0;
            string[] arreglo1 = { "°", "!", "\"", "$", "%", "(", ")", "=", "?", "¿", "'", "¡", "+", "*", "[", "]", "_", "-", ".", "<", ">", ";", ",", ":", "´", "¨", "|", "#", "&", "." ,"@"};
            char[] calis = dato.ToArray();

            for (int i = 0; i < 31; i++)
            {
                for (int ii = 0; ii < calis.Length; ii++)
                {
                    if (arreglo1[i] == calis[ii].ToString())
                    {
                        v = 1;
                        break;
                    }
                }
            }
            return v;
        }

        public int ValidarCantidadCaracteres(string dato)
        {
            int v = 0;
            char[] calis = dato.ToArray();

            for (int i = 0; i < calis.Length; i++)
            {
                v++;
            }
            return v;
        }
    }
}
