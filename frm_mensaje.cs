﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoVerano
{
    public partial class frm_mensaje : Form
    {
        public frm_mensaje(string menaje)
        {
            InitializeComponent();
            txtContenido.Text = menaje;
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
