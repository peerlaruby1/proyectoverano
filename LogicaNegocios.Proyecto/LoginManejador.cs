﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.Proyecto;
using AccesoDatos.Proyecto;

namespace LogicaNegocios.Proyecto
{
    public class LoginManejador
    {
        private LoginAccesoDatos _lac = new LoginAccesoDatos();
        public string login(string usuario, string contraseña)
        {
            var variable = _lac.login(usuario, contraseña);
            return variable;
        }
    }
}
