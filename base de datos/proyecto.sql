drop database proyecto;
create database proyecto;
use proyecto;

create table usuarios(
usuario varchar(10) primary key,
contraseņa varchar(1000)
);

create table cliente(
idcliente int primary key auto_increment,
nombre varchar(100) 
);

create table productos(
idproducto int primary key auto_increment,
nombre varchar(100),
precio double
);

create table almacen(
idalmacen int primary key auto_increment,
idproducto int,
cantidad int,
foreign key(idproducto) references productos(idproducto)
);

create table ventas(
idventas int primary key auto_increment,
idproducto int,
cantidad int,
costadounitatio double,
costatotal double,
fechareserva varchar(50),
fechaentrega varchar(50),
foreign key (idproducto) references productos(idproducto)
);

create table compras(
idcompra int primary key auto_increment,
idinsumo int,
precio double,
cantidad double,
fecha varchar(50)
);

create table venta(
idventa int primary key auto_increment,
idcliente int,
fkidventa int,
costototal double,
fechareserva varchar(50),
fechaentrega varchar(50),
foreign key (idcliente) references cliente(idcliente),
foreign key (fkidventa) references ventas(idventas)
);

create table produccion(
idproducto int primary key auto_increment,
fkproducto int,
fkinsumo int,
fkrol int,
cantidad int,
estatus int,
foreign key (fkproducto) references productos(idproducto)
);

create table insumos(
idinsumo int primary key auto_increment,
nombre varchar(100),
cantidad int
);

create table rol(
rol int primary key auto_increment,
nombre varchar(100)
);


create table personal(
idpersonal int primary key auto_increment,
nombre varchar(100),
apellido varchar(100),
correo varchar(100),
direccion varchar(100),
fkrol int,
foreign key (fkrol) references rol(rol)
);

insert into usuarios values('perla1','a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3'); /*la contraseņa es:123*/

/*Procedimiento almacenado logueo*/
create PROCEDURE validarlogin(
in _usuario varchar(10),
in _contraseņa varchar(1000))
begin
declare x int;
select count(*) from usuarios where usuario = _usuario and contraseņa = _contraseņa into x;
select x;
end;


